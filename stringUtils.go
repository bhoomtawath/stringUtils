// string-utils project string-utils.go
package stringUtils

func Reverse(str string) string {
	var (
		result []rune;
		runes  []rune = []rune(str)
	)
	for i, j := len(str)-1, 0; i >= 0; i, j = i-1, j+1 {
		result = append(result, runes[i])
	}

	return string(result)
}

func SubString(str string, begin int, end int) string {
	return str[begin: end]
}

func SubStr(str string, begin int, length int) string {
	var endIndex int = begin + length
	return SubString(str, begin, endIndex)
}
